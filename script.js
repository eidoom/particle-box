const canvas_sim = document.getElementById('simulation');
const context_sim = canvas_sim.getContext('2d', {
    alpha: false,
});
canvas_sim.width = 500;
canvas_sim.height = 500;

const canvas_hist = document.getElementById('histo');
const context_hist = canvas_hist.getContext('2d', {
    alpha: false,
});
canvas_hist.width = 500;
canvas_hist.height = 300;

let dt = 1;
let number = 100;
let speed = 3;
let radius = 5;
let colour = 'red';

function random(min, max) {
    return Math.random() * (max - min) + min;
}

let particles = [];

class Particle {
    constructor(x, y, vx, vy, r, c) {
        this.x = x;
        this.y = y;
        this.vx = vx;
        this.vy = vy;
        this.radius = r;
        this.colour = c;
    }

    speed() {
        return Math.hypot(this.vx, this.vy);
    }

    energy() {
        return 0.5 * (this.vx ** 2 + this.vy ** 2);
    }

    collide(obstacle) {
        let nx = this.x - obstacle.x;
        let ny = this.y - obstacle.y;
        const sep = Math.hypot(nx, ny);
        const x0 = this.radius + obstacle.radius;
        if (sep < x0) {
            nx /= sep;
            ny /= sep;

            const dot = (this.vx - obstacle.vx) * nx + (this.vy - obstacle.vy) * ny;

            this.vx -= dot * nx;
            this.vy -= dot * ny;

            obstacle.vx += dot * nx;
            obstacle.vy += dot * ny;

            this.x = obstacle.x + nx * x0;
            this.y = obstacle.y + ny * x0;
        }
    }

    update() {
        if (this.y > canvas_sim.height - this.radius || this.y < this.radius) {
            this.vy *= -1;
        }

        if (this.x > canvas_sim.height - this.radius || this.x < this.radius) {
            this.vx *= -1;
        }

        this.x += dt * this.vx;
        this.y += dt * this.vy;

        for (const particle of particles) {
            if (particle !== this) {
                this.collide(particle);
            }
        }
    }

    draw(ctx) {
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true);
        ctx.closePath();
        ctx.fillStyle = this.colour;
        ctx.fill();
    }
}

class Histogram {
    bins;

    constructor(start, end, num, cvs) {
        this.start = start;
        this.end = end;
        const span = this.end - this.start;
        this.num = num;
        this.width = span / this.num;
        this.boundaries = Array.from({
            length: this.num + 1
        }, (_, k) => this.width * k + this.start);
        this.x_scale = cvs.width / span;
        this.y_scale = 3 * cvs.height / number;
    }

    update(pcls) {
        this.bins = Array(this.num).fill(0);
        for (let pcl of pcls) {
            const en = pcl.energy();
            for (let i = 0; i < this.num; i++) {
                if (en > this.boundaries[i] && en < this.boundaries[i + 1]) {
                    this.bins[i] += 1;
                    break;
                }
            }
        }
    }

    draw(ctx, cvs) {
        ctx.fillStyle = 'green';
        for (let i = 0; i < this.num; i++) {
            const height = this.y_scale * this.bins[i]
            ctx.fillRect(this.x_scale * this.boundaries[i], cvs.height - height, this.x_scale * this.width, height);
        }
    }
}

for (let i = 0; i < number; i++) {
    const angle = random(0, Math.PI);
    particles.push(new Particle(
        random(2 * radius, canvas_sim.width - 2 * radius),
        random(2 * radius, canvas_sim.height - 2 * radius),
        speed * Math.cos(angle),
        speed * Math.sin(angle),
        radius,
        i === 0 ? 'blue' : 'red'
    ));
}

let histo = new Histogram(0, 20, 40, canvas_hist);

function draw() {
    context_sim.fillStyle = 'white';
    context_sim.fillRect(0, 0, canvas_sim.width, canvas_sim.height);

    for (let particle of particles) {
        particle.update();
        particle.draw(context_sim);
    }

    context_hist.fillStyle = 'white';
    context_hist.fillRect(0, 0, canvas_hist.width, canvas_hist.height);

    histo.update(particles);
    histo.draw(context_hist, canvas_hist);

    window.requestAnimationFrame(draw);
}

draw();
