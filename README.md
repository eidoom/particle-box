# [particle-box](https://gitlab.com/eidoom/particle-box)

* [Live](https://eidoom.gitlab.io/particle-box/)

## Physics
* https://en.wikipedia.org/wiki/Verlet_integration#Velocity_Verlet
* https://en.wikipedia.org/wiki/Elastic_collision#Two-dimensional_collision_with_two_moving_objects
